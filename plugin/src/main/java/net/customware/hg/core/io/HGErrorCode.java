package net.customware.hg.core.io;

import java.io.*;
import java.util.Map;
import java.util.HashMap;

public class HGErrorCode
    implements Serializable
{

    public static HGErrorCode getErrorCode(int code)
    {
        HGErrorCode errorCode = (HGErrorCode)ourErrorCodes.get(new Integer(code));
        if(errorCode == null)
            errorCode = UNKNOWN;
        return errorCode;
    }

    protected HGErrorCode(int category, int index, String description)
    {
        myCategory = category;
        myCode = category + index;
        myDescription = description;
        ourErrorCodes.put(new Integer(myCode), this);
    }

    public int getCode()
    {
        return myCode;
    }

    public int getCategory()
    {
        return myCategory;
    }

    public String getDescription()
    {
        return myDescription;
    }

    public int hashCode()
    {
        return myCode;
    }

    public boolean equals(Object o)
    {
        if(o == null || o.getClass() != (net.customware.hg.core.io.HGErrorCode.class))
            return false;
        else
            return myCode == ((HGErrorCode)o).myCode;
    }

    public boolean isAuthentication()
    {
        return this == RA_NOT_AUTHORIZED || this == RA_UNKNOWN_AUTH || getCategory() == 0x35b60 || getCategory() == 0x347d8;
    }

    private void writeObject(ObjectOutputStream os)
        throws IOException
    {
        os.writeInt(myCode);
    }

    private void readObject(ObjectInputStream is)
        throws IOException
    {
        myCode = is.readInt();
    }

    private Object readResolve()
    {
        return ourErrorCodes.get(new Integer(myCode));
    }

    public String toString()
    {
        return myCode + ": " + myDescription;
    }

    private String myDescription;
    private int myCategory;
    private int myCode;
    private static final Map ourErrorCodes = new HashMap();

    // TODO: NOTE: these values are all svn not hg values

    private static final int ERR_BASE = 0x1d4c0;
    private static final int ERR_CATEGORY_SIZE = 5000;
    public static final int BAD_CATEGORY = 0x1e848;
    public static final int XML_CATEGORY = 0x1fbd0;
    public static final int IO_CATEGORY = 0x20f58;
    public static final int STREAM_CATEGORY = 0x222e0;
    public static final int NODE_CATEGORY = 0x23668;
    public static final int ENTRY_CATEGORY = 0x249f0;
    public static final int WC_CATEGORY = 0x25d78;
    public static final int FS_CATEGORY = 0x27100;
    public static final int REPOS_CATEGORY = 0x28488;
    public static final int RA_CATEGORY = 0x29810;
    public static final int RA_DAV_CATEGORY = 0x2ab98;
    public static final int RA_LOCAL_CATEGORY = 0x2bf20;
    public static final int SVNDIFF_CATEGORY = 0x2d2a8;
    public static final int APMOD_CATEGORY = 0x2e630;
    public static final int CLIENT_CATEGORY = 0x2f9b8;
    public static final int MISC_CATEGORY = 0x30d40;
    public static final int CL_CATEGORY = 0x320c8;
    public static final int RA_SVN_CATEGORY = 0x33450;
    public static final int AUTHN_CATEGORY = 0x347d8;
    public static final int AUTHZ_CATEGORY = 0x35b60;
    public static final HGErrorCode UNKNOWN = new HGErrorCode(0x30d40, 4900, "Unknown error");
    public static final HGErrorCode IO_ERROR = new HGErrorCode(0x30d40, 4899, "Generic IO error");
    public static final HGErrorCode BAD_CONTAINING_POOL = new HGErrorCode(0x1e848, 0, "Bad parent pool passed to svn_make_pool()");
    public static final HGErrorCode BAD_FILENAME = new HGErrorCode(0x1e848, 1, "Bogus filename");
    public static final HGErrorCode BAD_URL = new HGErrorCode(0x1e848, 2, "Bogus URL");
    public static final HGErrorCode BAD_DATE = new HGErrorCode(0x1e848, 3, "Bogus date");
    public static final HGErrorCode BAD_MIME_TYPE = new HGErrorCode(0x1e848, 4, "Bogus mime-type");
    public static final HGErrorCode BAD_PROPERTY_VALUE = new HGErrorCode(0x1e848, 5, "Wrong or unexpected property value");
    public static final HGErrorCode BAD_VERSION_FILE_FORMAT = new HGErrorCode(0x1e848, 6, "Version file format not correct");
    public static final HGErrorCode BAD_RELATIVE_PATH = new HGErrorCode(0x1e848, 7, "Path is not an immediate child of the specified directory");
    public static final HGErrorCode BAD_UUID = new HGErrorCode(0x1e848, 8, "Bogus UUID");
    public static final HGErrorCode XML_ATTRIB_NOT_FOUND = new HGErrorCode(0x1fbd0, 0, "No such XML tag attribute");
    public static final HGErrorCode XML_MISSING_ANCESTRY = new HGErrorCode(0x1fbd0, 1, "<delta-pkg> is missing ancestry");
    public static final HGErrorCode XML_UNKNOWN_ENCODING = new HGErrorCode(0x1fbd0, 2, "Unrecognized binary data encoding; can't decode");
    public static final HGErrorCode XML_MALFORMED = new HGErrorCode(0x1fbd0, 3, "XML data was not well-formed");
    public static final HGErrorCode XML_UNESCAPABLE_DATA = new HGErrorCode(0x1fbd0, 4, "Data cannot be safely XML-escaped");
    public static final HGErrorCode IO_INCONSISTENT_EOL = new HGErrorCode(0x20f58, 0, "Inconsistent line ending style");
    public static final HGErrorCode IO_UNKNOWN_EOL = new HGErrorCode(0x20f58, 1, "Unrecognized line ending style");
    public static final HGErrorCode IO_CORRUPT_EOL = new HGErrorCode(0x20f58, 2, "Line endings other than expected");
    public static final HGErrorCode IO_UNIQUE_NAMES_EXHAUSTED = new HGErrorCode(0x20f58, 3, "Ran out of unique names");
    public static final HGErrorCode IO_PIPE_FRAME_ERROR = new HGErrorCode(0x20f58, 4, "Framing error in pipe protocol");
    public static final HGErrorCode IO_PIPE_READ_ERROR = new HGErrorCode(0x20f58, 5, "Read error in pipe");
    public static final HGErrorCode IO_WRITE_ERROR = new HGErrorCode(0x20f58, 6, "Write error");
    public static final HGErrorCode STREAM_UNEXPECTED_EOF = new HGErrorCode(0x222e0, 0, "Unexpected EOF on stream");
    public static final HGErrorCode STREAM_MALFORMED_DATA = new HGErrorCode(0x222e0, 1, "Malformed stream data");
    public static final HGErrorCode STREAM_UNRECOGNIZED_DATA = new HGErrorCode(0x222e0, 2, "Unrecognized stream data");
    public static final HGErrorCode NODE_UNKNOWN_KIND = new HGErrorCode(0x23668, 0, "Unknown svn_node_kind");
    public static final HGErrorCode NODE_UNEXPECTED_KIND = new HGErrorCode(0x23668, 1, "Unexpected node kind found");
    public static final HGErrorCode ENTRY_NOT_FOUND = new HGErrorCode(0x249f0, 0, "Can't find an entry");
    public static final HGErrorCode ENTRY_EXISTS = new HGErrorCode(0x249f0, 2, "Entry already exists");
    public static final HGErrorCode ENTRY_MISSING_REVISION = new HGErrorCode(0x249f0, 3, "Entry has no revision");
    public static final HGErrorCode ENTRY_MISSING_URL = new HGErrorCode(0x249f0, 4, "Entry has no URL");
    public static final HGErrorCode ENTRY_ATTRIBUTE_INVALID = new HGErrorCode(0x249f0, 5, "Entry has an invalid attribute");
    public static final HGErrorCode WC_OBSTRUCTED_UPDATE = new HGErrorCode(0x25d78, 0, "Obstructed update");
    public static final HGErrorCode WC_UNWIND_MISMATCH = new HGErrorCode(0x25d78, 1, "Mismatch popping the WC unwind stack");
    public static final HGErrorCode WC_UNWIND_EMPTY = new HGErrorCode(0x25d78, 2, "Attempt to pop empty WC unwind stack");
    public static final HGErrorCode WC_UNWIND_NOT_EMPTY = new HGErrorCode(0x25d78, 3, "Attempt to unlock with non-empty unwind stack");
    public static final HGErrorCode WC_LOCKED = new HGErrorCode(0x25d78, 4, "Attempted to lock an already-locked dir");
    public static final HGErrorCode WC_NOT_LOCKED = new HGErrorCode(0x25d78, 5, "Working copy not locked; this is probably a bug, please report");
    public static final HGErrorCode WC_INVALID_LOCK = new HGErrorCode(0x25d78, 6, "Invalid lock");
    public static final HGErrorCode WC_NOT_DIRECTORY = new HGErrorCode(0x25d78, 7, "Path is not a working copy directory");
    public static final HGErrorCode WC_NOT_FILE = new HGErrorCode(0x25d78, 8, "Path is not a working copy file");
    public static final HGErrorCode WC_BAD_ADM_LOG = new HGErrorCode(0x25d78, 9, "Problem running log");
    public static final HGErrorCode WC_PATH_NOT_FOUND = new HGErrorCode(0x25d78, 10, "Can't find a working copy path");
    public static final HGErrorCode WC_NOT_UP_TO_DATE = new HGErrorCode(0x25d78, 11, "Working copy is not up-to-date");
    public static final HGErrorCode WC_LEFT_LOCAL_MOD = new HGErrorCode(0x25d78, 12, "Left locally modified or unversioned files");
    public static final HGErrorCode WC_SCHEDULE_CONFLICT = new HGErrorCode(0x25d78, 13, "Unmergeable scheduling requested on an entry");
    public static final HGErrorCode WC_PATH_FOUND = new HGErrorCode(0x25d78, 14, "Found a working copy path");
    public static final HGErrorCode WC_FOUND_CONFLICT = new HGErrorCode(0x25d78, 15, "A conflict in the working copy obstructs the current operation");
    public static final HGErrorCode WC_CORRUPT = new HGErrorCode(0x25d78, 16, "Working copy is corrupt");
    public static final HGErrorCode WC_CORRUPT_TEXT_BASE = new HGErrorCode(0x25d78, 17, "Working copy text base is corrupt");
    public static final HGErrorCode WC_NODE_KIND_CHANGE = new HGErrorCode(0x25d78, 18, "Cannot change node kind");
    public static final HGErrorCode WC_INVALID_OP_ON_CWD = new HGErrorCode(0x25d78, 19, "Invalid operation on the current working directory");
    public static final HGErrorCode WC_BAD_ADM_LOG_START = new HGErrorCode(0x25d78, 20, "Problem on first log entry in a working copy");
    public static final HGErrorCode WC_UNSUPPORTED_FORMAT = new HGErrorCode(0x25d78, 21, "Unsupported working copy format");
    public static final HGErrorCode WC_BAD_PATH = new HGErrorCode(0x25d78, 22, "Path syntax not supported in this context");
    public static final HGErrorCode WC_INVALID_SCHEDULE = new HGErrorCode(0x25d78, 23, "Invalid schedule");
    public static final HGErrorCode WC_INVALID_RELOCATION = new HGErrorCode(0x25d78, 24, "Invalid relocation");
    public static final HGErrorCode WC_INVALID_SWITCH = new HGErrorCode(0x25d78, 25, "Invalid switch");
    public static final HGErrorCode WC_MISMATCHED_CHANGELIST = new HGErrorCode(0x25d78, 26, "Changelist doesn't match");
    public static final HGErrorCode WC_CONFLICT_RESOLVER_FAILURE = new HGErrorCode(0x25d78, 27, "Conflict resolution failed");
    public static final HGErrorCode WC_COPYFROM_PATH_NOT_FOUND = new HGErrorCode(0x25d78, 28, "Failed to locate 'copyfrom' path in working copy");
    public static final HGErrorCode WC_CHANGELIST_MOVE = new HGErrorCode(0x25d78, 29, "Moving a path from one changelist to another");
    public static final HGErrorCode FS_GENERAL = new HGErrorCode(0x27100, 0, "General filesystem error");
    public static final HGErrorCode FS_CLEANUP = new HGErrorCode(0x27100, 1, "Error closing filesystem");
    public static final HGErrorCode FS_ALREADY_OPEN = new HGErrorCode(0x27100, 2, "Filesystem is already open");
    public static final HGErrorCode FS_NOT_OPEN = new HGErrorCode(0x27100, 3, "Filesystem is not open");
    public static final HGErrorCode FS_CORRUPT = new HGErrorCode(0x27100, 4, "Filesystem is corrupt");
    public static final HGErrorCode FS_PATH_SYNTAX = new HGErrorCode(0x27100, 5, "Invalid filesystem path syntax");
    public static final HGErrorCode FS_NO_SUCH_REVISION = new HGErrorCode(0x27100, 6, "Invalid filesystem revision number");
    public static final HGErrorCode FS_NO_SUCH_TRANSACTION = new HGErrorCode(0x27100, 7, "Invalid filesystem transaction name");
    public static final HGErrorCode FS_NO_SUCH_ENTRY = new HGErrorCode(0x27100, 8, "Filesystem directory has no such entry");
    public static final HGErrorCode FS_NO_SUCH_REPRESENTATION = new HGErrorCode(0x27100, 9, "Filesystem has no such representation");
    public static final HGErrorCode FS_NO_SUCH_STRING = new HGErrorCode(0x27100, 10, "Filesystem has no such string");
    public static final HGErrorCode FS_NO_SUCH_COPY = new HGErrorCode(0x27100, 11, "Filesystem has no such copy");
    public static final HGErrorCode FS_TRANSACTION_NOT_MUTABLE = new HGErrorCode(0x27100, 12, "The specified transaction is not mutable");
    public static final HGErrorCode FS_NOT_FOUND = new HGErrorCode(0x27100, 13, "Filesystem has no item");
    public static final HGErrorCode FS_ID_NOT_FOUND = new HGErrorCode(0x27100, 14, "Filesystem has no such node-rev-id");
    public static final HGErrorCode FS_NOT_ID = new HGErrorCode(0x27100, 15, "String does not represent a node or node-rev-id");
    public static final HGErrorCode FS_NOT_DIRECTORY = new HGErrorCode(0x27100, 16, "Name does not refer to a filesystem directory");
    public static final HGErrorCode FS_NOT_FILE = new HGErrorCode(0x27100, 17, "Name does not refer to a filesystem file");
    public static final HGErrorCode FS_NOT_SINGLE_PATH_COMPONENT = new HGErrorCode(0x27100, 18, "Name is not a single path component");
    public static final HGErrorCode FS_NOT_MUTABLE = new HGErrorCode(0x27100, 19, "Attempt to change immutable filesystem node");
    public static final HGErrorCode FS_ALREADY_EXISTS = new HGErrorCode(0x27100, 20, "Item already exists in filesystem");
    public static final HGErrorCode FS_ROOT_DIR = new HGErrorCode(0x27100, 21, "Attempt to remove or recreate fs root dir");
    public static final HGErrorCode FS_NOT_TXN_ROOT = new HGErrorCode(0x27100, 22, "Object is not a transaction root");
    public static final HGErrorCode FS_NOT_REVISION_ROOT = new HGErrorCode(0x27100, 23, "Object is not a revision root");
    public static final HGErrorCode FS_CONFLICT = new HGErrorCode(0x27100, 24, "Merge conflict during commit");
    public static final HGErrorCode FS_REP_CHANGED = new HGErrorCode(0x27100, 25, "A representation vanished or changed between reads");
    public static final HGErrorCode FS_REP_NOT_MUTABLE = new HGErrorCode(0x27100, 26, "Tried to change an immutable representation");
    public static final HGErrorCode FS_MALFORMED_SKEL = new HGErrorCode(0x27100, 27, "Malformed skeleton data");
    public static final HGErrorCode FS_TXN_OUT_OF_DATE = new HGErrorCode(0x27100, 28, "Transaction is out of date");
    public static final HGErrorCode FS_BERKELEY_DB = new HGErrorCode(0x27100, 29, "Berkeley DB error");
    public static final HGErrorCode FS_BERKELEY_DB_DEADLOCK = new HGErrorCode(0x27100, 30, "Berkeley DB deadlock error");
    public static final HGErrorCode FS_TRANSACTION_DEAD = new HGErrorCode(0x27100, 31, "Transaction is dead");
    public static final HGErrorCode FS_TRANSACTION_NOT_DEAD = new HGErrorCode(0x27100, 32, "Transaction is not dead");
    public static final HGErrorCode FS_UNKNOWN_FS_TYPE = new HGErrorCode(0x27100, 33, "Unknown FS type");
    public static final HGErrorCode FS_NO_USER = new HGErrorCode(0x27100, 34, "No user associated with filesystem");
    public static final HGErrorCode FS_PATH_ALREADY_LOCKED = new HGErrorCode(0x27100, 35, "Path is already locked");
    public static final HGErrorCode FS_PATH_NOT_LOCKED = new HGErrorCode(0x27100, 36, "Path is not locked");
    public static final HGErrorCode FS_BAD_LOCK_TOKEN = new HGErrorCode(0x27100, 37, "Lock token is incorrect");
    public static final HGErrorCode FS_NO_LOCK_TOKEN = new HGErrorCode(0x27100, 38, "No lock token provided");
    public static final HGErrorCode FS_LOCK_OWNER_MISMATCH = new HGErrorCode(0x27100, 39, "Username does not match lock owner");
    public static final HGErrorCode FS_NO_SUCH_LOCK = new HGErrorCode(0x27100, 40, "Filesystem has no such lock");
    public static final HGErrorCode FS_LOCK_EXPIRED = new HGErrorCode(0x27100, 41, "Lock has expired");
    public static final HGErrorCode FS_OUT_OF_DATE = new HGErrorCode(0x27100, 42, "Item is out of date");
    public static final HGErrorCode FS_UNSUPPORTED_FORMAT = new HGErrorCode(0x27100, 43, "Unsupported FS format");
    public static final HGErrorCode FS_REP_BEING_WRITTEN = new HGErrorCode(0x27100, 44, "Representation is being written");
    public static final HGErrorCode FS_TXN_NAME_TOO_LONG = new HGErrorCode(0x27100, 45, "The generated transaction name is too long");
    public static final HGErrorCode FS_NO_SUCH_NODE_ORIGIN = new HGErrorCode(0x27100, 46, "Filesystem has no such node origin record");
    public static final HGErrorCode REPOS_LOCKED = new HGErrorCode(0x28488, 0, "The repository is locked, perhaps for db recovery");
    public static final HGErrorCode REPOS_HOOK_FAILURE = new HGErrorCode(0x28488, 1, "A repository hook failed");
    public static final HGErrorCode REPOS_BAD_ARGS = new HGErrorCode(0x28488, 2, "Incorrect arguments supplied");
    public static final HGErrorCode REPOS_NO_DATA_FOR_REPORT = new HGErrorCode(0x28488, 3, "A report cannot be generated because no data was supplied");
    public static final HGErrorCode REPOS_BAD_REVISION_REPORT = new HGErrorCode(0x28488, 4, "Bogus revision report");
    public static final HGErrorCode REPOS_UNSUPPORTED_VERSION = new HGErrorCode(0x28488, 5, "Unsupported repository version");
    public static final HGErrorCode REPOS_DISABLED_FEATURE = new HGErrorCode(0x28488, 6, "Disabled repository feature");
    public static final HGErrorCode REPOS_POST_COMMIT_HOOK_FAILED = new HGErrorCode(0x28488, 7, "Error running post-commit hook");
    public static final HGErrorCode REPOS_POST_LOCK_HOOK_FAILED = new HGErrorCode(0x28488, 8, "Error running post-lock hook");
    public static final HGErrorCode REPOS_POST_UNLOCK_HOOK_FAILED = new HGErrorCode(0x28488, 9, "Error running post-unlock hook");
    public static final HGErrorCode RA_ILLEGAL_URL = new HGErrorCode(0x29810, 0, "Bad URL passed to RA layer");
    public static final HGErrorCode RA_NOT_AUTHORIZED = new HGErrorCode(0x29810, 1, "Authorization failed");
    public static final HGErrorCode RA_UNKNOWN_AUTH = new HGErrorCode(0x29810, 2, "Unknown authorization method");
    public static final HGErrorCode RA_NOT_IMPLEMENTED = new HGErrorCode(0x29810, 3, "Repository access method not implemented");
    public static final HGErrorCode RA_OUT_OF_DATE = new HGErrorCode(0x29810, 4, "Item is out-of-date");
    public static final HGErrorCode RA_NO_REPOS_UUID = new HGErrorCode(0x29810, 5, "Repository has no UUID");
    public static final HGErrorCode RA_UNSUPPORTED_ABI_VERSION = new HGErrorCode(0x29810, 6, "Unsupported RA plugin ABI version");
    public static final HGErrorCode RA_NOT_LOCKED = new HGErrorCode(0x29810, 7, "Path is not locked");
    public static final HGErrorCode RA_PARTIAL_REPLAY_NOT_SUPPORTED = new HGErrorCode(0x29810, 8, "Server can only replay from the root of a repository");
    public static final HGErrorCode RA_UUID_MISMATCH = new HGErrorCode(0x29810, 9, "Repository UUID does not match expected UUID");
    public static final HGErrorCode RA_DAV_SOCK_INIT = new HGErrorCode(0x2ab98, 0, "RA layer failed to init socket layer");
    public static final HGErrorCode RA_DAV_CREATING_REQUEST = new HGErrorCode(0x2ab98, 1, "RA layer failed to create HTTP request");
    public static final HGErrorCode RA_DAV_REQUEST_FAILED = new HGErrorCode(0x2ab98, 2, "RA layer request failed");
    public static final HGErrorCode RA_DAV_OPTIONS_REQ_FAILED = new HGErrorCode(0x2ab98, 3, "RA layer didn't receive requested OPTIONS info");
    public static final HGErrorCode RA_DAV_PROPS_NOT_FOUND = new HGErrorCode(0x2ab98, 4, "RA layer failed to fetch properties");
    public static final HGErrorCode RA_DAV_ALREADY_EXISTS = new HGErrorCode(0x2ab98, 5, "RA layer file already exists");
    public static final HGErrorCode RA_DAV_INVALID_CONFIG_VALUE = new HGErrorCode(0x2ab98, 6, "Invalid configuration value");
    public static final HGErrorCode RA_DAV_PATH_NOT_FOUND = new HGErrorCode(0x2ab98, 7, "HTTP Path Not Found");
    public static final HGErrorCode RA_DAV_PROPPATCH_FAILED = new HGErrorCode(0x2ab98, 8, "Failed to execute WebDAV PROPPATCH");
    public static final HGErrorCode RA_DAV_MALFORMED_DATA = new HGErrorCode(0x2ab98, 9, "Malformed network data");
    public static final HGErrorCode RA_DAV_RESPONSE_HEADER_BADNESS = new HGErrorCode(0x2ab98, 10, "Unable to extract data from response header");
    public static final HGErrorCode RA_LOCAL_REPOS_NOT_FOUND = new HGErrorCode(0x2bf20, 0, "Couldn't find a repository");
    public static final HGErrorCode RA_LOCAL_REPOS_OPEN_FAILED = new HGErrorCode(0x2bf20, 1, "Couldn't open a repository");
    public static final HGErrorCode RA_SVN_CMD_ERR = new HGErrorCode(0x33450, 0, "Special code for wrapping server errors to report to client");
    public static final HGErrorCode RA_SVN_UNKNOWN_CMD = new HGErrorCode(0x33450, 1, "Unknown svn protocol command");
    public static final HGErrorCode RA_SVN_CONNECTION_CLOSED = new HGErrorCode(0x33450, 2, "Network connection closed unexpectedly");
    public static final HGErrorCode RA_SVN_IO_ERROR = new HGErrorCode(0x33450, 3, "Network read/write error");
    public static final HGErrorCode RA_SVN_MALFORMED_DATA = new HGErrorCode(0x33450, 4, "Malformed network data");
    public static final HGErrorCode RA_SVN_REPOS_NOT_FOUND = new HGErrorCode(0x33450, 5, "Couldn't find a repository");
    public static final HGErrorCode RA_SVN_BAD_VERSION = new HGErrorCode(0x33450, 6, "Client/server version mismatch");
    public static final HGErrorCode AUTHN_CREDS_UNAVAILABLE = new HGErrorCode(0x347d8, 0, "Credential data unavailable");
    public static final HGErrorCode AUTHN_NO_PROVIDER = new HGErrorCode(0x347d8, 1, "No authentication provider available");
    public static final HGErrorCode AUTHN_PROVIDERS_EXHAUSTED = new HGErrorCode(0x347d8, 2, "All authentication providers exhausted");
    public static final HGErrorCode AUTHN_CREDS_NOT_SAVED = new HGErrorCode(0x347d8, 3, "Credentials not saved");
    public static final HGErrorCode AUTHZ_ROOT_UNREADABLE = new HGErrorCode(0x35b60, 0, "Read access denied for root of edit");
    public static final HGErrorCode AUTHZ_UNREADABLE = new HGErrorCode(0x35b60, 1, "Item is not readable");
    public static final HGErrorCode AUTHZ_PARTIALLY_READABLE = new HGErrorCode(0x35b60, 2, "Item is partially readable");
    public static final HGErrorCode AUTHZ_INVALID_CONFIG = new HGErrorCode(0x35b60, 3, "Invalid authz configuration");
    public static final HGErrorCode AUTHZ_UNWRITABLE = new HGErrorCode(0x35b60, 4, "Item is not writable");
    public static final HGErrorCode SVNDIFF_INVALID_HEADER = new HGErrorCode(0x2d2a8, 0, "Svndiff data has invalid header");
    public static final HGErrorCode SVNDIFF_CORRUPT_WINDOW = new HGErrorCode(0x2d2a8, 1, "Svndiff data contains corrupt window");
    public static final HGErrorCode SVNDIFF_BACKWARD_VIEW = new HGErrorCode(0x2d2a8, 2, "Svndiff data contains backward-sliding source view");
    public static final HGErrorCode SVNDIFF_INVALID_OPS = new HGErrorCode(0x2d2a8, 3, "Svndiff data contains invalid instruction");
    public static final HGErrorCode SVNDIFF_UNEXPECTED_END = new HGErrorCode(0x2d2a8, 4, "Svndiff data ends unexpectedly");
    public static final HGErrorCode APMOD_MISSING_PATH_TO_FS = new HGErrorCode(0x2e630, 0, "Apache has no path to an SVN filesystem");
    public static final HGErrorCode APMOD_MALFORMED_URI = new HGErrorCode(0x2e630, 1, "Apache got a malformed URI");
    public static final HGErrorCode APMOD_ACTIVITY_NOT_FOUND = new HGErrorCode(0x2e630, 2, "Activity not found");
    public static final HGErrorCode APMOD_BAD_BASELINE = new HGErrorCode(0x2e630, 3, "Baseline incorrect");
    public static final HGErrorCode APMOD_CONNECTION_ABORTED = new HGErrorCode(0x2e630, 4, "Input/output error");
    public static final HGErrorCode CLIENT_VERSIONED_PATH_REQUIRED = new HGErrorCode(0x2f9b8, 0, "A path under version control is needed for this operation");
    public static final HGErrorCode CLIENT_RA_ACCESS_REQUIRED = new HGErrorCode(0x2f9b8, 1, "Repository access is needed for this operation");
    public static final HGErrorCode CLIENT_BAD_REVISION = new HGErrorCode(0x2f9b8, 2, "Bogus revision information given");
    public static final HGErrorCode CLIENT_DUPLICATE_COMMIT_URL = new HGErrorCode(0x2f9b8, 3, "Attempting to commit to a URL more than once");
    public static final HGErrorCode CLIENT_IS_BINARY_FILE = new HGErrorCode(0x2f9b8, 4, "Operation does not apply to binary file");
    public static final HGErrorCode CLIENT_INVALID_EXTERNALS_DESCRIPTION = new HGErrorCode(0x2f9b8, 5, "Format of an svn:externals property was invalid");
    public static final HGErrorCode CLIENT_MODIFIED = new HGErrorCode(0x2f9b8, 6, "Attempting restricted operation for modified resource");
    public static final HGErrorCode CLIENT_IS_DIRECTORY = new HGErrorCode(0x2f9b8, 7, "Operation does not apply to directory");
    public static final HGErrorCode CLIENT_REVISION_RANGE = new HGErrorCode(0x2f9b8, 8, "Revision range is not allowed");
    public static final HGErrorCode CLIENT_INVALID_RELOCATION = new HGErrorCode(0x2f9b8, 9, "Inter-repository relocation not allowed");
    public static final HGErrorCode CLIENT_REVISION_AUTHOR_CONTAINS_NEWLINE = new HGErrorCode(0x2f9b8, 10, "Author name cannot contain a newline");
    public static final HGErrorCode CLIENT_PROPERTY_NAME = new HGErrorCode(0x2f9b8, 11, "Bad property name");
    public static final HGErrorCode CLIENT_UNRELATED_RESOURCES = new HGErrorCode(0x2f9b8, 12, "Two versioned resources are unrelated");
    public static final HGErrorCode CLIENT_MISSING_LOCK_TOKEN = new HGErrorCode(0x2f9b8, 13, "Path has no lock token");
    public static final HGErrorCode CLIENT_MULTIPLE_SOURCES_DISALLOWED = new HGErrorCode(0x2f9b8, 14, "Operation does not support multiple sources");
    public static final HGErrorCode CLIENT_NO_VERSIONED_PARENT = new HGErrorCode(0x2f9b8, 15, "No versioned parent directories");
    public static final HGErrorCode CLIENT_NOT_READY_TO_MERGE = new HGErrorCode(0x2f9b8, 16, "Working copy and merge source not ready for reintegration");
    public static final HGErrorCode BASE = new HGErrorCode(0x30d40, 0, "A problem occurred; see later errors for details");
    public static final HGErrorCode PLUGIN_LOAD_FAILURE = new HGErrorCode(0x30d40, 1, "Failure loading plugin");
    public static final HGErrorCode MALFORMED_FILE = new HGErrorCode(0x30d40, 2, "Malformed file");
    public static final HGErrorCode INCOMPLETE_DATA = new HGErrorCode(0x30d40, 3, "Incomplete data");
    public static final HGErrorCode INCORRECT_PARAMS = new HGErrorCode(0x30d40, 4, "Incorrect parameters given");
    public static final HGErrorCode UNVERSIONED_RESOURCE = new HGErrorCode(0x30d40, 5, "Tried a versioning operation on an unversioned resource");
    public static final HGErrorCode TEST_FAILED = new HGErrorCode(0x30d40, 6, "Test failed");
    public static final HGErrorCode UNSUPPORTED_FEATURE = new HGErrorCode(0x30d40, 7, "Trying to use an unsupported feature");
    public static final HGErrorCode BAD_PROP_KIND = new HGErrorCode(0x30d40, 8, "Unexpected or unknown property kind");
    public static final HGErrorCode ILLEGAL_TARGET = new HGErrorCode(0x30d40, 9, "Illegal target for the requested operation");
    public static final HGErrorCode DELTA_MD5_CHECKSUM_ABSENT = new HGErrorCode(0x30d40, 10, "MD5 checksum is missing");
    public static final HGErrorCode DIR_NOT_EMPTY = new HGErrorCode(0x30d40, 11, "Directory needs to be empty but is not");
    public static final HGErrorCode EXTERNAL_PROGRAM = new HGErrorCode(0x30d40, 12, "Error calling external program");
    public static final HGErrorCode SWIG_PY_EXCEPTION_SET = new HGErrorCode(0x30d40, 13, "Python exception has been set with the error");
    public static final HGErrorCode CHECKSUM_MISMATCH = new HGErrorCode(0x30d40, 14, "A checksum mismatch occurred");
    public static final HGErrorCode CANCELLED = new HGErrorCode(0x30d40, 15, "The operation was interrupted");
    public static final HGErrorCode INVALID_DIFF_OPTION = new HGErrorCode(0x30d40, 16, "The specified diff option is not supported");
    public static final HGErrorCode PROPERTY_NOT_FOUND = new HGErrorCode(0x30d40, 17, "Property not found");
    public static final HGErrorCode NO_AUTH_FILE_PATH = new HGErrorCode(0x30d40, 18, "No auth file path available");
    public static final HGErrorCode VERSION_MISMATCH = new HGErrorCode(0x30d40, 19, "Incompatible library version");
    public static final HGErrorCode MERGE_INFO_PARSE_ERROR = new HGErrorCode(0x30d40, 20, "Merge info parse error");
    public static final HGErrorCode CEASE_INVOCATION = new HGErrorCode(0x30d40, 21, "Cease invocation of this API");
    public static final HGErrorCode REVISION_NUMBER_PARSE_ERROR = new HGErrorCode(0x30d40, 22, "Revision number parse error");
    public static final HGErrorCode ITER_BREAK = new HGErrorCode(0x30d40, 23, "Iteration terminated before completion");
    public static final HGErrorCode UNKNOWN_CHANGELIST = new HGErrorCode(0x30d40, 24, "Unknown changelist");
    public static final HGErrorCode RESERVED_FILENAME_SPECIFIED = new HGErrorCode(0x30d40, 25, "Reserved directory name in command line arguments");
    public static final HGErrorCode UNKNOWN_CAPABILITY = new HGErrorCode(0x30d40, 26, "Inquiry about unknown capability");
    public static final HGErrorCode CL_ARG_PARSING_ERROR = new HGErrorCode(0x320c8, 0, "Client error in parsing arguments");
    public static final HGErrorCode CL_INSUFFICIENT_ARGS = new HGErrorCode(0x320c8, 1, "Not enough args provided");
    public static final HGErrorCode CL_MUTUALLY_EXCLUSIVE_ARGS = new HGErrorCode(0x320c8, 2, "Mutually exclusive arguments specified");
    public static final HGErrorCode CL_ADM_DIR_RESERVED = new HGErrorCode(0x320c8, 3, "Attempted command in administrative dir");
    public static final HGErrorCode CL_LOG_MESSAGE_IS_VERSIONED_FILE = new HGErrorCode(0x320c8, 4, "The log message file is under version control");
    public static final HGErrorCode CL_LOG_MESSAGE_IS_PATHNAME = new HGErrorCode(0x320c8, 5, "The log message is a pathname");
    public static final HGErrorCode CL_COMMIT_IN_ADDED_DIR = new HGErrorCode(0x320c8, 6, "Committing in directory scheduled for addition");
    public static final HGErrorCode CL_NO_EXTERNAL_EDITOR = new HGErrorCode(0x320c8, 7, "No external editor available");
    public static final HGErrorCode CL_BAD_LOG_MESSAGE = new HGErrorCode(0x320c8, 8, "Something is wrong with the log message's contents");
    public static final HGErrorCode CL_UNNECESSARY_LOG_MESSAGE = new HGErrorCode(0x320c8, 9, "A log message was given where none was necessary");
    public static final HGErrorCode CL_NO_EXTERNAL_MERGE_TOOL = new HGErrorCode(0x320c8, 10, "No external merge tool available");

}
