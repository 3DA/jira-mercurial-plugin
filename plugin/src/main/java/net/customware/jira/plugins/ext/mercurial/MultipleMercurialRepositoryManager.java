package net.customware.jira.plugins.ext.mercurial;

import net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexer;

import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.issue.index.IndexException;

import java.io.IOException;
import java.util.Collection;

/**
 * Main component of the Mercurial plugin.
 */
public interface MultipleMercurialRepositoryManager extends Startable
{
    String HG_ROOT_KEY = "hg.root";
    String HG_CLONEDIR_KEY = "hg.clonedir";
    String HG_RELEASENOTESEMAIL_KEY = "hg.releasenotesemail";
    String HG_RELEASENOTESLATEST_KEY = "hg.releasenoteslatest";
    String HG_SUBREPOSCMD_KEY = "hg.subreposcmd";
    String HG_REPOSITORY_NAME = "hg.display.name";
    String HG_USERNAME_KEY = "hg.username";
    String HG_PASSWORD_KEY = "hg.password";
    String HG_PRIVATE_KEY_FILE = "hg.privatekeyfile";
    String HG_SUBREPOS_KEY = "hg.subrepos";
    String HG_REVISION_INDEXING_KEY = "revision.indexing";
    String HG_REVISION_CACHE_SIZE_KEY = "revision.cache.size";
    // Note that the repositoryUUID value is not stored

    String HG_LINKFORMAT_TYPE = "linkformat.type";
    String HG_LINKFORMAT_CHANGESET = "linkformat.changeset";
    String HG_LINKFORMAT_FILE_ADDED = "linkformat.file.added";
    String HG_LINKFORMAT_FILE_MODIFIED = "linkformat.file.modified";
    String HG_LINKFORMAT_FILE_REPLACED = "linkformat.file.replaced";
    String HG_LINKFORMAT_FILE_DELETED = "linkformat.file.deleted";

    String HG_LINKFORMAT_PATH_KEY = "linkformat.copyfrom";

    String HG_LOG_MESSAGE_CACHE_SIZE_KEY = "logmessage.cache.size";

    boolean isIndexingRevisions();

    RevisionIndexer getRevisionIndexer();

    /**
     * Returns a Collection of MercurialManager instances, one for
     * each repository.
     *
     * @return the managers.
     */
    Collection<MercurialManager> getRepositoryList();

    Collection<MercurialManager> getSubrepositoriesList();

    Collection<MercurialManager> getActiveRepositoryList();

    Collection<MercurialManager> getInactiveRepositoryList();


    MercurialManager getRepository(long repoId);

    MercurialManager getRepository(String repoName);

    MercurialManager createRepository(HgProperties props, boolean isSubrepos);

    MercurialManager updateRepository(long repoId, HgProperties props);

    void reindexRepository(long repoId) throws IndexException, IOException;

    void removeRepository(long repoId);

    public void checkSubrepos(MercurialManager mercurialManager);
}
